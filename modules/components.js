import { capitalize } from "./utils.js";

let simpleRules = [];

function component(componentType, name, { label, options = [], hint, inputs = [] } = {}) {
    if (!name) return;
    if (!label) label = name;

    let html = `<form onSubmit="return false;" class="reForm"><fieldset>
                <legend>${game.i18n.localize("PF2e-REG." + label)}</legend>`;

    switch (componentType) {
        case "text":
        case "number":
            if (!hint) simpleRules.push(`${name}`);
            html += field(componentType, name, { hint });
            break;
        case "select":
            simpleRules.push(`${name}`);
            html += field(componentType, name, { hint, options });
            break;
        case "multipleText":
            html += `<p>${hint}<br /></p>`;
            for (let input of inputs) {
                if (typeof input == "string") input = [input];
                if (!input[1]) input[1] = input[0] + ":";
                html += field("text", input[0], { label: input[1] });
            }
            break;
        case "multipleSelect":
            html += `<p>${hint}<br /></p>`;
            for (let input of inputs) {
                if (typeof input == "string") input = [input];
                if (!input[1]) input[1] = input[0] + ":";
                html += field("select", ...input);
            }
            break;
        case "multipleInput":
            if (hint) html += `<p>${hint}<br /></p>`;
            for (let input of inputs) {
                if (!input[2]?.label && !input[2]?.hint)
                    input[2] ? (input[2].label = input[1]) : (input[2] = { label: input[1] });
                html += field(...input);
            }
    }

    html += `</fieldset></form>`;
    return html;
}
function field(fieldType, name, { label, hint, options = [] }) {
    let html = "";
    if (label) html += `<label>${game.i18n.localize("PF2e-REG." + label)}</label>`;
    if (hint) {
        if (typeof hint == "string") html += `<p>${game.i18n.localize("PF2e-REG." + hint)}</p>`;
        else for (let text of hint) html += `<p>${game.i18n.localize("PF2e-REG." + text)}</p>`;
    }
    switch (fieldType) {
        case "text":
        case "number":
        case "checkbox":
            html += `<input type="${fieldType}" id="re${name}">`;
            break;
        case "select":
            html += `<select id="re${name}">`;
            for (let option of options) {
                html += makeOption(option);
            }
            html += "</select>";
            break;
    }
    return html;
}

function makeOption(option) {
    if (typeof option == "string") option = [option];
    return `<option value="${option[0]}">${game.i18n.localize(
        "PF2e-REG." + (option[1] || capitalize(option[0]))
    )}</option>`;
}

const selectorValues = {
    leading: [
        "",
        "all",
        ["str-based", "Strength-Based"],
        ["dex-based", "Dexterity-Based"],
        ["con-based", "Constitution-Based"],
        ["int-based", "Intelligence-Based"],
        ["wis-based", "Wisdom-Based"],
        ["cha-based", "Charisma-Based"],
        "attack",
        ["mundane-attack", "Mundane Attack"],
        ["spell-attack", "Spell Attack"],
        ["str-attack", "Strength Attack"],
        ["dex-attack", "Dexterity Attack"],
        ["con-attack", "Constitution Attack"],
        ["int-attack", "Intelligence Attack"],
        ["wis-attack", "Wisdom Attack"],
        ["cha-attack", "Charisma Attack"],
    ],
    damage: ["damage", ["mundane-damage", "Mundane Damage"]],
    speeds: [
        ["speed", "All Speeds"],
        ["land-speed", "Land Speed"],
        ["burrow-speed", "Burrow Speed"],
        ["climb-speed", "Climb Speed"],
        ["fly-speed", "Fly Speed"],
        ["swim-speed", "Swim Speed"],
    ],
    others: [
        ["saving-throw", "Saving Throws"],
        "fortitude",
        "reflex",
        "will",
        "initiative",
        "perception",
        ["class", "Class DC"],
        ["ac", "Armor Class"],
        ["hp", "Hit Points"],
        ["hp-per-level", "HP per Level"],
        ["speed", "All Speeds"],
        ["land-speed", "Land Speed"],
        ["burrow-speed", "Burrow Speed"],
        ["climb-speed", "Climb Speed"],
        ["fly-speed", "Fly Speed"],
        ["swim-speed", "Swim Speed"],
        ["skill-check", "Skill Check"],
        "acrobatics",
        "arcana",
        "athletics",
        "crafting",
        "deception",
        "diplomacy",
        "intimidation",
        "medicine",
        "nature",
        "occultism",
        "performance",
        "religion",
        "society",
        "stealth",
        "survival",
        "thievery",
    ],
};
selectorValues.all = selectorValues.leading.concat(selectorValues.damage, selectorValues.speeds, selectorValues.others);
const label = component("text", "Label");
const relabel = component("text", "ReLabel");
const text = component("text", "Text");
const anyType = component("text", "Type");
const value = component("text", "Value");
const textValue = component("text", "Value");
const path = component("text", "Path");
const scope = component("text", "Scope");
const flag = component("text", "Flag");
const except = component("text", "Except");
const category = component("text", "Category");
const slug = component("text", "Slug");
const domain = component("text", "Domain");
const option = component("text", "Option");
const property = component("text", "Property");
const priority = component("text", "Priority");
const promptText = component("text", "Prompt");
const uuid = component("text", "Uuid", { label: "uuid" });
const targetId = component("text", "TargetId", { label: "Target ID" });
const traits = component("text", "Traits", { hint: "Separate entries with a comma (no space)" });
const add = component("text", "Add", { hint: "Separate entries with a comma (no space)" });
const range = component("number", "Range");

const rollOptions = component("text", "RollOptions", {
    label: "Roll Options",
    hint: "Separate entries with a comma (no space)",
});
const choices = component("text", "Choices", {
    hint: [
        "Format: label1,value1,label2,value2,label3,value3",
        "Example: PF2e.TraitFire,fire,PF2e.TraitWater,water,PF2e.TraitEarth,earth,PF2e.TraitAir,air",
        "Labels may be omitted",
        "Example: ,fire,,water,,earth,,air",
    ],
});

const selector = component("select", "Selector", { options: selectorValues.all });
const damageSelector = component("select", "Selector", { options: ["", ...selectorValues.damage] });
const selectorAttack = component("select", "Selector", { options: ["attack"] });
const selectorDamage = component("select", "Selector", { options: ["damage"] });
const speedSelector = component("select", "Selector", { options: ["land", "burrow", "climb", "fly", "swim"] });
const type = component("select", "Type", { options: ["", "untyped", "circumstance", "item", "status"] });
const adjustType = component("select", "Type", { options: ["", "attribute", "save", "skill"] });
const critical = component("select", "Critical", { options: ["", "true"] });
const alternate = component("select", "Alternate", { options: ["", "true"] });
const keep = component("select", "Keep", { options: ["higher", "lower"] });
const mode = component("select", "Mode", { options: ["", "multiply", "add", "downgrade", "upgrade", "override"] });
const addMode = component("select", "Mode", { options: ["add"] });
const acuity = component("select", "Acuity", { options: ["", "precise", "imprecise", "vague"] });
const replaceSelf = component("select", "ReplaceSelf", { label: "Replace Self", options: ["", "true", "false"] });
const adjustName = component("select", "AdjustName", { label: "Adjust Name", options: ["", "true", "false"] });

const senseSelector = component("select", "Selector", {
    options: [
        "",
        ["lowLightVision", "Low-Light Vision"],
        "darkvision",
        ["greater-darkvision", "Greater Darkvision"],
        "scent",
        "blindsight",
        "tremorsense",
        "echolocation",
        "lifesense",
        "motionsense",
        "wavesense",
    ],
});
const advSelector = component("select", "AdvSelector", {
    label: "Advanced Selector",
    options: [
        "",
        ["{item|_id}-", "This Item"],
        ["{item|data.target}-", "Target Item"],
        ["sword-weapon-group-", "Swords"],
        ["bow-weapon-group-", "Bows"],
        ["knife-weapon-group-", "Knives"],
        ["spear-weapon-group-", "Spears"],
        ["axe-weapon-group-", "Axes"],
        ["hammer-weapon-group-", "Hammers"],
        ["pick-weapon-group-", "Picks"],
        ["flail-weapon-group-", "Flails"],
        ["brawling-weapon-group-", "Brawling"],
        ["polearm-weapon-group-", "Polearms"],
        ["sling-weapon-group-", "Slings"],
        ["bomb-weapon-group-", "Bombs"],
        ["shield-weapon-group-", "Shields"],
        ["dart-weapon-group-", "Darts"],
        ["club-weapon-group-", "Club"],
    ],
});
const potencyAdvSelector = component("select", "AdvSelector", {
    label: "Advanced Selector",
    options: ["", ["{item|_id}-", "This Item"]],
});
const strikingAdvSelector = component("select", "AdvSelector", {
    label: "Advanced Selector",
    options: ["", ["{item|_id}-", "This Item"]],
});
const damageType = component("select", "DamageType", {
    label: "Damage Type",
    options: [
        "",
        "slashing",
        "piercing",
        "bludgeoning",
        "precision",
        "fire",
        "cold",
        "electricity",
        "acid",
        "poison",
        "sonic",
        "force",
        "mental",
        "lawful",
        "chaotic",
        "good",
        "evil",
        "positive",
        "negative",
    ],
});
const sizeValue = component("select", "Value", {
    options: ["", "tiny", "small", "medium", "large", "huge", "gargantuan"],
});
const group = component("select", "Group", {
    options: [
        "",
        "sword",
        "bow",
        "knife",
        "spear",
        "axe",
        "hammer",
        "pick",
        "flail",
        "brawling",
        "polearm",
        "sling",
        "bomb",
        "shield",
        "dart",
        "club",
    ],
});

const definition = component("multipleText", "Definition", {
    hint: "Separate entries with a comma (no space)",
    inputs: [
        ["DefAll", "All:"],
        ["DefAny", "Any:"],
        ["DefNot", "Not:"],
    ],
});
const allowedDrops = component("multipleText", "Allowed Drops", {
    inputs: [
        ["ADLabel", "Label"],
        ["ADAll", "All:"],
        ["ADNot", "Not:"],
    ],
});
const predicate = component("multipleText", "Predicates", {
    hint: "Separate entries with a comma (no space)",
    inputs: ["All", "Any", "Not"],
});
const adjustment = component("multipleSelect", "Adjustment", {
    label: "Adjustments",
    inputs: [
        [
            "Crit",
            {
                label: "Critical Success",
                options: ["", ["one-degree-worse", "One Degree Worse"], ["two-degrees-worse", "Two Degrees Worse"]],
            },
        ],
        [
            "Success",
            {
                label: "Success",
                options: [
                    "",
                    ["one-degree-better", "One Degree Better"],
                    ["one-degree-worse", "One Degree Worse"],
                    ["two-degrees-worse", "Two Degrees Worse"],
                ],
            },
        ],
        [
            "Fail",
            {
                label: "Failure",
                options: [
                    "",
                    ["two-degrees-better", "Two Degrees Better"],
                    ["one-degree-better", "One Degree Better"],
                    ["one-degree-worse", "One Degree Worse"],
                ],
            },
        ],
        [
            "Fumble",
            {
                label: "Critical Failure",
                options: ["", ["two-degrees-better", "Two Degrees Better"], ["one-degree-better", "One Degree Better"]],
            },
        ],
    ],
});

const lightValue =
    component("multipleInput", "Configuration", {
        inputs: [
            ["number", "Bright"],
            ["number", "Dim"],
            ["text", "Color"],
            ["number", "Shadows"],
        ],
    }) +
    component("multipleInput", "Animation", {
        inputs: [
            ["number", "Intensity"],
            ["number", "Speed"],
            ["text", "Type"],
        ],
    });

const brackets = component("multipleInput", "Brackets", {
    inputs: [
        [
            "text",
            "Brackets",
            {
                hint: [
                    "Format: start1,value1,end1,start2,value2,end2",
                    "Example: 1,1,9,10,2,17,18,3,20",
                    "First start and last end may be omitted",
                    "Example: ,1,9,10,2,17,18,3",
                ],
            },
        ],
        ["text", "Field", { label: "Field (for start and end to reference something other than actor level):" }],
        ["checkbox", "BracketDiceNumber", { label: "Use diceNumber" }],
    ],
});

`
    <form onSubmit="return false;" class="reForm"><fieldset>
        <legend>${game.i18n.localize("Brackets")}</legend>
        ${field("text", "Brackets", {
            hint: [
                "Format: start1,value1,end1,start2,value2,end2",
                "Example: 1,1,9,10,2,17,18,3,20",
                "First start and last end may be omitted",
                "Example: ,1,9,10,2,17,18,3",
            ],
        })}
        ${field("text", "Field", { hint: "Field (for start and end to reference something other than actor level):" })}
        ${field("checkbox", "BracketDiceNumber", { label: "Use diceNumber" })}
    </fieldset></form>`;
const outcome = `
    <form onSubmit="return false;" class="reForm"><fieldset>
        <legend>${game.i18n.localize("Outcome")}</legend>
        <label>${game.i18n.localize("Critical Success")}</label>
        <input type="checkbox" id="reOutcomeCrit">

        <label>${game.i18n.localize("Success")}</label>
        <input type="checkbox" id="reOutcomeSuccess">
        <br />
        <label>${game.i18n.localize("Failure")}</label>
        <input type="checkbox" id="reOutcomeFail">

        <label>${game.i18n.localize("Critical Failure")}</label>
        <input type="checkbox" id="reOutcomeFumble">
    </fieldset></form>`;
const dice = `
    <form onSubmit="return false;" class="reForm"><fieldset>
        <legend>${game.i18n.localize("Dice")}</legend>
        <label>${game.i18n.localize("Number of Dice")}</label>
        <input type="number" id="reDiceNumber">
        <label>${game.i18n.localize("Die Size")}</label>
        <select id="reDieSize">
            <option value=""></option>
            <option value="d4">d4</option>
            <option value="d6">d6</option>
            <option value="d8">d8</option>
            <option value="d10">d10</option>
            <option value="d12">d12</option>
            <option value="d20">d20</option>
        </select>
        <label>${game.i18n.localize("Override Current Dice Size")}</label>
        <input type="checkbox" id="reOverride">
    </fieldset></form>`;
const damage = `
    <form onSubmit="return false;" class="reForm"><fieldset>
        <legend>${game.i18n.localize("Damage")}</legend>
        <label>${game.i18n.localize("Number of Dice")}</label>
        <input type="number" id="reDice">
        <label>${game.i18n.localize("Die Size")}</label>
        <select id="reDie">
            <option value=""></option>
            <option value="d4","d4</option>
            <option value="d6","d6</option>
            <option value="d8","d8</option>
            <option value="d10">d10</option>
            <option value="d12">d12</option>
            <option value="d20">d20</option>
        </select>
        <label>${game.i18n.localize("Damage Type")}</label>
        <select id="reDamage">
            <option value=""></option>
            <option value="slashing">Slashing</option>
            <option value="piercing">Piercing</option>
            <option value="bludgeoning">Bludgeoning</option>
            <option value="precision">Precision</option>
            <option value="fire">Fire</option>
            <option value="cold">Cold</option>
            <option value="electricity">Electricity</option>
            <option value="acid">Acid</option>
            <option value="poison">Poison</option>
            <option value="sonic">Sonic</option>
            <option value="force">Force</option>
            <option value="mental">Mental</option>
            <option value="lawful">Lawful</option>
            <option value="chaotic">Chaotic</option>
            <option value="good">Good</option>
            <option value="evil">Evil</option>
            <option value="positive">Positive</option>
            <option value="negative">Negative</option>
        </select>
    </fieldset></form>`;

const contents = {
    FlatModifier: label + selector + advSelector + type + value + predicate + brackets,
    Immunity: anyType + predicate,
    Weakness: anyType + value + predicate,
    Resistance: anyType + value + except + predicate,
    FastHealing: value,
    DamageDice: label + damageSelector + advSelector + damageType + dice + traits + predicate + critical + brackets,
    BaseSpeed: label + speedSelector + value + predicate + brackets,
    FixedProficiency: label + selector + advSelector + value + predicate,
    Strike: label + category + group + damage + range + traits,
    Note: text + selector + advSelector + predicate + outcome,
    DexterityModifierCap: value + predicate,
    Sense: label + senseSelector + acuity + range,
    WeaponPotency: selectorAttack + potencyAdvSelector + value + predicate,
    Striking: selectorDamage + strikingAdvSelector + value + predicate,
    MultipleAttackPenalty: label + selectorAttack + advSelector + value + rollOptions + predicate,
    LoseHitPoints: value + predicate,
    AdjustStrike: value + addMode + property + definition + predicate,
    AdjustModifier: selector + value + mode + relabel + slug,
    TokenLight: lightValue,
    CriticalSpecialization: alternate + text + predicate,
    MartialProficiency: label + slug + value + definition,
    ActiveEffectLike: mode + path + value + priority + predicate + brackets,
    RollOption: domain + option,
    ChoiceSet: label + promptText + flag + adjustName + choices + allowedDrops,
    GrantItem: replaceSelf + uuid + predicate,
    TempHP: textValue,
    TokenEffectIcon: textValue,
    TokenImage: textValue,
    CreatureSize: sizeValue,
    EffectTarget: scope + targetId,
    ActorTraits: add,
    RollTwice: keep + selector,
    AdjustDegreeOfSuccess: label + adjustType + selector + adjustment + predicate,
};

export { contents, simpleRules };
